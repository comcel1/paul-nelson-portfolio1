## Title:
My First Portfolio

## Github Repository Title:
paul-nelson-portfolio1
## Purpose:
Bootcamp challenge #2: My First Portfolio

## Built with:
* HTML
* CSS

## Website:
hosted: <!-- https://comcel1.github.io/horiseon-refactor-project/ > </br>
github: <!-- https://github.com/comcel1/horiseon-refactor-project >

## Screenshot
![screenshot] <!-- (https://user-images.githubusercontent.com/90969624/146689938-d00a2c11-82df-434b-aadf-a315f94cd998.png) >

## Contribution
Submitted by Paul Nelson </br>
<!--December 26, 2021 >
